using Events;
using UnityEngine;
using UnityEngine.Events;

public class SoundManager : MonoBehaviour
{
    AudioSource audioFx;
    public AudioClip correctAnswer;
    public AudioClip wrongAnswer;
    public AudioClip endOfSerie;
    public AudioClip pass;
    public AudioClip reset;

    private EventBusSingleton _eventBus;

    void OnEnable() {
        _eventBus = EventBusSingleton.Instance;
        _eventBus.espManagerEvents.validAnswerEvent.onSent += Play(correctAnswer);
        _eventBus.espManagerEvents.wrongAnswerEvent.onSent+=Play(wrongAnswer);
        _eventBus.espManagerEvents.endOfSerieAnswerEvent.onSent += Play(endOfSerie);
        _eventBus.uiEvents.passEvent.onSent += Play(pass);
        _eventBus.uiEvents.resetEvent.onSent += Play(reset);
    }

    void OnDisable() {
        _eventBus.espManagerEvents.validAnswerEvent.onSent -= Play(correctAnswer);
        _eventBus.espManagerEvents.wrongAnswerEvent.onSent -= Play(wrongAnswer);
        _eventBus.espManagerEvents.endOfSerieAnswerEvent.onSent -= Play(endOfSerie);
        _eventBus.uiEvents.passEvent.onSent -= Play(pass);
        _eventBus.uiEvents.resetEvent.onSent -= Play(reset);
    }

    void Start()
    {
        audioFx = GetComponent<AudioSource>();
    }

    UnityAction Play(AudioClip clip) {
        return () => {
            audioFx.clip = clip;
            audioFx.Play(0);
        };
    }
}
