using UnityEngine;
using UnityEngine.Events;

public class FloatChannel
{
    public UnityAction<float> onSent;
    
    public void RaiseEvent(float value)
    {
        if(onSent != null)
        {
            onSent.Invoke(value);
        }
        else
        {
            Debug.LogWarning("Float event was sent but not catched");
        }
    }
}
