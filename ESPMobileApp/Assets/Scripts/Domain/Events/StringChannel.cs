using UnityEngine;
using UnityEngine.Events;

public class StringChannel
{
    public UnityAction<string> onSent;

    public void RaiseEvent(string value)
    {
        if(onSent != null)
        {
            onSent.Invoke(value);
        }
        else
        {
            Debug.LogWarning("String event was sent but not catched");
        }
    }
}
