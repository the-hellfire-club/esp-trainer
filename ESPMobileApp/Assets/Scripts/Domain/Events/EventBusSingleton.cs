﻿using UnityEngine;

namespace Events
{
    public class EventBusSingleton
    {
        private static EventBusSingleton _instance;
        
        public static EventBusSingleton Instance
        {
            // ajout ET création du composant à un GameObject nommé "EventListHolder" 
            //get { return instance ?? (instance = new GameObject("EventListHolder").AddComponent<EventBusSingleton>());}
            get { return _instance ??= new EventBusSingleton(); }
            private set => _instance = value;
        }

        public readonly EspManagerEvents espManagerEvents = new();
        public readonly UiEvents uiEvents = new();
    }

    public class EspManagerEvents
    {
        public readonly VoidChannel endOfSerieAnswerEvent = new();
        public readonly VoidChannel wrongAnswerEvent = new();
        public readonly VoidChannel validAnswerEvent = new();
        public readonly IntChannel newImageGeneratedEvent = new();
        public readonly FloatChannel scoreChangedEvent = new();
        //TODO : devrait être la responsabilité de l'UI
        public readonly StringChannel displayTipsEvent = new();
    }

    public class UiEvents
    {
        public readonly IntChannel passedWithValueEvent = new();
        public readonly IntChannel answerSelectedEvent = new();
        public readonly VoidChannel resetEvent = new();
        public readonly VoidChannel passEvent = new();
        public readonly VoidChannel startPressedEvent = new();
        public readonly VoidChannel closeImageEvent = new();

        public UiEvents()
        {
            Debug.Log("Event bus instantiated");
        }
    }
}