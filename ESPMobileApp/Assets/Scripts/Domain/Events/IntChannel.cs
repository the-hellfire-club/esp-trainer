using UnityEngine;
using UnityEngine.Events;

public class IntChannel
{
    public UnityAction<int> onSent;
    
    public void RaiseEvent(int value)
    {
        if(onSent != null)
        {
            onSent.Invoke(value);
        }
        else
        {
            Debug.LogWarning("Int event was sent but not catched");
        }
    }
}
