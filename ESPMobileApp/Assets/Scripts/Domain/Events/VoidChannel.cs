using UnityEngine;
using UnityEngine.Events;

public class VoidChannel
{
    public UnityAction onSent;

    public void RaiseEvent()
    {
        if(onSent != null)
        {
            onSent.Invoke();
        }
        else
        {
            Debug.LogWarning("Void event was sent but not catched");
        }
    }
}
