﻿using System;
using UnityEngine;

namespace Events
{
    [CreateAssetMenu(menuName = "Events/EventBus")]
    public class EventBusScriptableObject : ScriptableObject
    {
        private EventBusSingleton _eventBus;

        public void OnEnable()
        {
            _eventBus = EventBusSingleton.Instance;
        }

        public void RaisePassEvent() => _eventBus.uiEvents.passEvent.RaiseEvent();
        public void RaiseResetEvent() => _eventBus.uiEvents.resetEvent.RaiseEvent();
        public void RaiseAnswerSelectedEvent(int selectedAnswer) => _eventBus.uiEvents.answerSelectedEvent.RaiseEvent(selectedAnswer);
        public void RaisePlayEvent() => _eventBus.uiEvents.startPressedEvent.RaiseEvent();
        public void RaiseCloseImageEvent() => _eventBus.uiEvents.closeImageEvent.RaiseEvent();
    }
}