using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class DataController {
    private int _valueToGuess;
    public int ValueToGuess {get { return _valueToGuess; }}
    private List<int> _randomSerie = new();
    private List<int> _responses = new();
    private DateTimeOffset _sessionStartDate;

    private const int MaxValue = 3;
    private float TotalTrials { get; }
    private RandomGenerator _rng;

    public DataController(RandomGenerator rng, int numberOfTrials) {
        TotalTrials = numberOfTrials;
        _rng = rng;
    }

    public int Start() {
        _valueToGuess = _rng.Generate(0, MaxValue);
        _sessionStartDate = DateTimeOffset.Now;

        return _rng.Generate(0, 22);
    }

    public int GenerateNewValueAndImage() {
        _valueToGuess = _rng.Generate(0, MaxValue);
        Debug.Log("value generated "+_valueToGuess);

        return _rng.Generate(0, 22);
    }

    public int SaveAndRestart() {
      //_statsRepo.Save(_sessionStartDate, DateTimeOffset.Now, randomSerie, responses);

      _randomSerie.Clear();
      _responses.Clear();
        
      return GenerateNewValueAndImage();
    }

    public bool SaveAnswerAndGetResult(int answer) {
        _randomSerie.Add(_valueToGuess);
        _responses.Add(answer);

        return _valueToGuess == answer;
    }

    public int GetCurrentSessionCount() {
        return _randomSerie.Count();
    }

    public float ComputeScore() {
        return _responses.Where((resp, index) => resp == _randomSerie[index]).Select((_) => 1.0f/TotalTrials).Sum();
    }

    public string ExportScoreToJson(float score) {

        string path = Path.Combine(Application.dataPath, "../score.json");
        ScoresWrapper scoresWrapper;

        if (File.Exists(path)) {
            string existingJson = File.ReadAllText(path);
            scoresWrapper = JsonUtility.FromJson<ScoresWrapper>(existingJson);
        } else {
            scoresWrapper = new ScoresWrapper { Scores = new List<ScoreData>() };
        }

        string currentDate = DateTime.Now.ToString("dd-MM-yyyy");
        string currentTime = DateTime.Now.ToString("HH:mm:ss");

        ScoreData scoreData = new ScoreData {
            Score = score,
            Date = currentDate,
            Time = currentTime
        };

        scoresWrapper.Scores.Add(scoreData);

        string json = JsonUtility.ToJson(scoresWrapper);
        File.WriteAllText(path, json);

        Debug.Log("JSON mis-à-jour à cette adresse postale : " + path);

        return path;
    }

}

public class RandomGenerator {
    public RandomGenerator(int seed = 0)
    {
        if (seed == 0) UnityEngine.Random.InitState((int)DateTime.Now.Ticks);
        else UnityEngine.Random.InitState(seed);
    }
    public int Generate(int from, int to) {
        return (int)Mathf.Floor(UnityEngine.Random.Range(from, to+1));
    }
}

[Serializable]

// Classes pour structurer le JSON parce qu'on est très organisé ici // 
public class ScoreData {
    public float Score;
    public string Date;
    public string Time;
}

[Serializable]

public class ScoresWrapper {
    public List<ScoreData> Scores;
}

