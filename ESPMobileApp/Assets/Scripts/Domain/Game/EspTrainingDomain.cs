﻿using Events;
using UnityEngine;

namespace Game
{
    public class EspTrainingDomain
    {
        private readonly EventBusSingleton _eventBus;
        private readonly DataController _dataController;
        private const int StartingScore = 0;
        private int _nbOfTrials;
        private int _lastDisplayedLevel = -1;
        public EspTrainingDomain(int nbOfTrials, EventBusSingleton eventBus, DataController dataCtrl)
        {
            _nbOfTrials = nbOfTrials;
            _dataController = dataCtrl;
            _eventBus = eventBus;
        }
        
        public void RegisterToEvents()
        {
            _eventBus.uiEvents.resetEvent.onSent += OnResetEvent;
            _eventBus.uiEvents.passEvent.onSent += OnPassEvent;
            _eventBus.uiEvents.answerSelectedEvent.onSent += OnAnswerSelected;
        }
        
        public void UnregisterToEvents()
        {
            _eventBus.uiEvents.resetEvent.onSent -= OnResetEvent;
            _eventBus.uiEvents.passEvent.onSent -= OnPassEvent;
            _eventBus.uiEvents.answerSelectedEvent.onSent -= OnAnswerSelected;
        }

        public void OnResetEvent()
        {
            _dataController.SaveAndRestart();
            _eventBus.uiEvents.passedWithValueEvent.RaiseEvent(-1);
            _eventBus.espManagerEvents.scoreChangedEvent.RaiseEvent(StartingScore);
        }

        public void OnStart()
        {
            _eventBus.espManagerEvents.newImageGeneratedEvent.RaiseEvent(_dataController.Start());
            _eventBus.espManagerEvents.scoreChangedEvent.RaiseEvent(StartingScore);
        }

        public void OnPassEvent()
        {
            _eventBus.espManagerEvents.newImageGeneratedEvent.RaiseEvent(_dataController.GenerateNewValueAndImage());
            _eventBus.uiEvents.passedWithValueEvent.RaiseEvent(_dataController.ValueToGuess);
        }

        public void OnAnswerSelected(int answer)
        {
            var isCorrect = _dataController.SaveAnswerAndGetResult(answer);
            
            if (isCorrect) _eventBus.espManagerEvents.validAnswerEvent.RaiseEvent();
            else _eventBus.espManagerEvents.wrongAnswerEvent.RaiseEvent();
            
            var score = _dataController.ComputeScore();
            DisplayTips(score);
            _eventBus.espManagerEvents.scoreChangedEvent.RaiseEvent(score);
            
            _eventBus.espManagerEvents.newImageGeneratedEvent.RaiseEvent(_dataController.GenerateNewValueAndImage());
            
            if (_dataController.GetCurrentSessionCount() < _nbOfTrials) return;
        
            _dataController.ExportScoreToJson(_dataController.ComputeScore());
            _dataController.SaveAndRestart();
            _eventBus.espManagerEvents.scoreChangedEvent.RaiseEvent(_dataController.ComputeScore());
            _eventBus.espManagerEvents.endOfSerieAnswerEvent.RaiseEvent();
        }
        
        private void DisplayTips(float score) {
            var scoreInt = (int)Mathf.Floor(score*_nbOfTrials);
            if (HasMessageAlreadyBeenDisplayed(scoreInt)) return;                 // Empêche le message de s'afficher plusieurs fois par pallier 
            switch(scoreInt) {
                case 6:
                    _eventBus.espManagerEvents.displayTipsEvent.RaiseEvent("A good start!");
                    break;
                case 7:
                    _eventBus.espManagerEvents.displayTipsEvent.RaiseEvent("Keep going!");
                    break;
                case 8:
                    _eventBus.espManagerEvents.displayTipsEvent.RaiseEvent("You're one with the force!");
                    break;
                case 9:
                    _eventBus.espManagerEvents.displayTipsEvent.RaiseEvent("And the force is with you!");
                    break;
                case 10:
                    _eventBus.espManagerEvents.displayTipsEvent.RaiseEvent("Psychic spy!");
                    break;
                case 11:
                    _eventBus.espManagerEvents.displayTipsEvent.RaiseEvent("Incredible perception!");
                    break;
                case 12:
                    _eventBus.espManagerEvents.displayTipsEvent.RaiseEvent("MacMoneagle? Is that you?");
                    break;
            }
        }

        private bool HasMessageAlreadyBeenDisplayed(int scoreInt)
        {
            if (scoreInt == _lastDisplayedLevel) return true;
            _lastDisplayedLevel = scoreInt;
            return false;
        }
    }
}