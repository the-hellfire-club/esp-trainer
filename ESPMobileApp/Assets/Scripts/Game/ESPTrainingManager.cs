using Events;
using UnityEngine;

namespace Game
{
    public class ESPTrainingManager : MonoBehaviour

    {
        private StatsRepository _statsRepo;
        private DataController _dataCtrl;
        
        private readonly EventBusSingleton _eventBus = EventBusSingleton.Instance;
        private EspTrainingDomain _espDomain;
        
        private int TotalTrials = 25;

        void OnEnable() {
            _espDomain.RegisterToEvents();
        }
        void OnDisable() {
            _espDomain.UnregisterToEvents();
        }
    
        // Start is called before the first frame update
        void Start()
        {
            var rng = new RandomGenerator();
            _dataCtrl = new DataController(rng, TotalTrials);
            _espDomain = new EspTrainingDomain(TotalTrials, _eventBus, _dataCtrl);

            _espDomain.RegisterToEvents();
            _espDomain.OnStart();
        }

        // Update is called once per frame
        void Update()
        {
        }
    }
}