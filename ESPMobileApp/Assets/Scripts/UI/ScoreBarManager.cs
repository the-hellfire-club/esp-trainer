using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBarManager : MonoBehaviour
{
    public Slider slider;

    private EventBusSingleton _eventBus;

    void OnEnable()
    {
        _eventBus = EventBusSingleton.Instance;
        _eventBus.espManagerEvents.scoreChangedEvent.onSent += OnScoreChanged;
    }

    void OnDisable() => _eventBus.espManagerEvents.scoreChangedEvent.onSent-=OnScoreChanged;
    // Start is called before the first frame update
    void Start()
    {
        slider = this.transform.GetComponent<Slider>();
        slider.value = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnScoreChanged(float newScoreNormalized) {
        if (slider == null) return;

        slider.value=newScoreNormalized;
    }
}
