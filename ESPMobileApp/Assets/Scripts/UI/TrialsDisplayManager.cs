using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine;
using TMPro;

public class TrialsDisplayManager : MonoBehaviour
{
    private EventBusSingleton _eventBus;

    private TextMeshProUGUI textHandler;
    private int nbOfResponses;

    public void OnEnable()
    {
        _eventBus = EventBusSingleton.Instance;
        _eventBus.uiEvents.answerSelectedEvent.onSent += OnAnswerSelected;
        _eventBus.uiEvents.resetEvent.onSent += OnResetEvent;
        _eventBus.espManagerEvents.endOfSerieAnswerEvent.onSent += OnEndOfSerie;
    }
    public void OnDisable() { 
        _eventBus.uiEvents.answerSelectedEvent.onSent -= OnAnswerSelected;
        _eventBus.uiEvents.resetEvent.onSent -= OnResetEvent;
        _eventBus.espManagerEvents.endOfSerieAnswerEvent.onSent -= OnEndOfSerie;
    }

    // Start is called before the first frame update
    void Start()
    {
        textHandler = transform.GetComponent<TextMeshProUGUI>();
        textHandler.SetText("0");
        nbOfResponses = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnResetEvent() {
        textHandler.SetText("0");
        nbOfResponses = 0;
    }

    void OnEndOfSerie() {
        nbOfResponses=-1; //OnAsnwerSelected called right after
        textHandler.SetText(nbOfResponses.ToString());
    }

    void OnAnswerSelected(int reponse) {
        nbOfResponses++;
        textHandler.SetText(nbOfResponses.ToString());
    }
}
