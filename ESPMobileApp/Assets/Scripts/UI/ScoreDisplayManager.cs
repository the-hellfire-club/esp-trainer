using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine;
using TMPro;

public class ScoreDisplayManager : MonoBehaviour
{
    public TextMeshProUGUI textHandler;

    private const int MAX_TRIALS = 25;
    private EventBusSingleton _eventBus;

    void OnEnable()
    {
        _eventBus = EventBusSingleton.Instance;
        _eventBus.espManagerEvents.scoreChangedEvent.onSent+=OnScoreChanged; 
    }
    void OnDisable() => _eventBus.espManagerEvents.scoreChangedEvent.onSent-=OnScoreChanged;
    // Start is called before the first frame update
    void Start()
    {
        textHandler = transform.GetComponent<TextMeshProUGUI>();
        textHandler.SetText("0");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnScoreChanged(float newScoreNormalized) {
        var newValue = Mathf.Round(newScoreNormalized*MAX_TRIALS); //OnAsnwerSelected called right after
        textHandler.SetText(newValue.ToString());
    }
}
