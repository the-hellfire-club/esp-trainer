using System.Collections;
using Events;
using UnityEngine;
using UnityEngine.UI;

public class RewardImageManager : MonoBehaviour
{
    public Sprite[] sprites;

    private EventBusSingleton _eventBus;
    private Image _rewardingImage;
    private Animator _animator;
    private const string path = "/Assets/Resources/Images/Pictures";

    void OnEnable() {
        _eventBus = EventBusSingleton.Instance;
        _eventBus.espManagerEvents.validAnswerEvent.onSent += OnCorrectAnswer;
        _eventBus.espManagerEvents.newImageGeneratedEvent.onSent+=OnNewImage;
        _eventBus.uiEvents.closeImageEvent.onSent+=OnCloseImage;
    }

    void OnDisable() {
        _eventBus.espManagerEvents.validAnswerEvent.onSent -= OnCorrectAnswer;
        _eventBus.espManagerEvents.newImageGeneratedEvent.onSent-=OnNewImage;
        _eventBus.uiEvents.closeImageEvent.onSent-=OnCloseImage;
    }
    void Start()
    {
        var imageGO = GameObject.Find("RewardImage");
        _rewardingImage = imageGO.GetComponent<Image>();
        _animator = imageGO.GetComponent<Animator>();
        Debug.Log(_rewardingImage);
        _rewardingImage.gameObject.SetActive(false);
        //slider = this.transform.GetComponent<Slider>();
    }

    void OnNewImage(int imageIndex) {
        Debug.Log("new image selected "+imageIndex);
        //_rewardingImage.sprite = Resources.Load<Sprite>($"{path}/{imageIndex}");
        _rewardingImage.sprite = sprites[imageIndex];
    }

    void OnCorrectAnswer() {
        _rewardingImage.gameObject.SetActive(true);
        _animator.SetTrigger("Start");
        StartCoroutine(Deactivate());
    }

    void OnCloseImage() {
        StopAllCoroutines();
        _animator.SetTrigger("Exit");
    }

    IEnumerator Deactivate() {
        yield return new WaitForSeconds(3);

        _animator.SetTrigger("Exit");

        yield return null;
    }


}
