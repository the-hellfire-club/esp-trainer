using System.Collections;
using Events;
using UnityEngine;
using UnityEngine.UI;

public class ButtonDisplayManager : MonoBehaviour
{
    public Button[] buttons;

    private EventBusSingleton _eventBus;
    
    void OnEnable() {
        _eventBus = EventBusSingleton.Instance;
        _eventBus.uiEvents.passedWithValueEvent.onSent += OnPassedEvent;
    }
    void OnDisable() {
        _eventBus.uiEvents.passedWithValueEvent.onSent += OnPassedEvent;
    }
    void OnPassedEvent(int valueToGuess)
    {
        Debug.Log("value to guess + "+valueToGuess);
        if (valueToGuess == -1)
        {
            HighlightAllButtons();
            return;
        }
        
        HighlightCorrectButton(valueToGuess);
    }
    void HighlightCorrectButton(int correctValue) {
        StartCoroutine(PlayAndResetAnimation(correctValue));
    }

    void HighlightAllButtons() {
        foreach (Button btn in buttons) {
            Debug.Log("J'allume les 4 boutons ! C'est beau la technologie.");
            StartCoroutine(PlayAndResetAnimationForButton(btn));
        }
    }
    
    // Routine d'animation d'un seul bouton
    IEnumerator PlayAndResetAnimation(int correctValue) {
        Animator btnAnimator = buttons[correctValue].GetComponent<Animator>();  
        btnAnimator.Play("Pressed");
        yield return new WaitForSeconds(0.2f); 
        btnAnimator.Play("Normal"); 
    }

    // Routine d'animation de tous les boutons
    IEnumerator PlayAndResetAnimationForButton(Button btn) {
        Animator btnAnimator = btn.GetComponent<Animator>();  
        btnAnimator.Play("Pressed");
        yield return new WaitForSeconds(0.1f); 
        btnAnimator.Play("Selected");
        yield return new WaitForSeconds(0.1f); 
        btnAnimator.Play("Normal");
    }
}
