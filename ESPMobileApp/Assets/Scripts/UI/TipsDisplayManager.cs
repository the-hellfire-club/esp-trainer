using System.Collections;
using Events;
using UnityEngine;
using TMPro;

public class TipsDisplay : MonoBehaviour {
    public TextMeshProUGUI encouragementText;
    private Coroutine hideTextCoroutine;

    private EventBusSingleton _eventBus;

    private void OnEnable() {
        _eventBus = EventBusSingleton.Instance;
        _eventBus.espManagerEvents.displayTipsEvent.onSent += UpdateEncouragementText;
    }

    private void OnDisable() {
        _eventBus.espManagerEvents.displayTipsEvent.onSent -= UpdateEncouragementText;
    }

    void UpdateEncouragementText(string message) {

        if (hideTextCoroutine != null) {                // Ecrase le message précédent si conflit
            StopCoroutine(hideTextCoroutine);
        }

        encouragementText.text = message;
        hideTextCoroutine = StartCoroutine(HideTextAfterDelay(2f));
    }

    IEnumerator HideTextAfterDelay(float delay) {
        yield return new WaitForSeconds(delay);
        encouragementText.text = "";
    }

}
