using Events;
using UnityEngine;

public class AppStateManager : MonoBehaviour
{
    private EventBusSingleton _eventBus;

    void OnEnable()
    {
        _eventBus = EventBusSingleton.Instance;
        _eventBus.uiEvents.startPressedEvent.onSent += OnStartEvent;
    }

    void OnDisable()
    {
        _eventBus.uiEvents.startPressedEvent.onSent -= OnStartEvent;
    }

    void OnStartEvent()
    {
        var menu = GameObject.Find("MainMenu");
        menu.SetActive(false);
    }
}
