using UnityEngine;
using UnityEngine.Events;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

[CreateAssetMenu(menuName = "Repositories/Statistics")]
public class StatsRepository : ScriptableObject
{
    private string path = "";
    public string filename = "stats";

    public void OnEnable()
    {
        path = Application.persistentDataPath + "/";
    }

    public void Save(DateTimeOffset sessionStartDate, DateTimeOffset sessionEndDate, List<int> randomGeneration, List<int> answers) {
        var dataToSave = new StatSession{
            sessionStartDate = sessionStartDate.ToString(),
            sessionEndDate = sessionEndDate.ToString(),
            randomGeneration = randomGeneration,
            answers = answers
        };

        Debug.Log("save file");

        SaveBinary(dataToSave);
        SaveJson(dataToSave);
    }

    private void SaveBinary(StatSession sessionToSave) {
        FileData dataToSave = null;
        using (var fs = File.Open(path+filename+".dat", FileMode.OpenOrCreate)) {
            if (fs.Length > 0) {
                var bf = new BinaryFormatter();
                dataToSave = (FileData) bf.Deserialize(fs);
            }
        }

        dataToSave = AppendData(dataToSave, sessionToSave);

        
        using (var fs = File.Open(path+filename+".dat", FileMode.OpenOrCreate)) {
            var bf = new BinaryFormatter();
            bf.Serialize(fs, dataToSave);
        }
    }

    private void SaveJson(StatSession sessionToSave) {
        FileData dataToSave = null;
        try {
            var fileContent = File.ReadAllText(path+filename+".json");
            dataToSave = JsonUtility.FromJson<FileData>(fileContent);
        } catch {}
        
        dataToSave = AppendData(dataToSave, sessionToSave);

        string json = JsonUtility.ToJson(dataToSave);
        File.WriteAllText(path+filename+".json", json);
    }

    private FileData AppendData(FileData readData, StatSession newData) {
        FileData result;
        if (readData == null) {
            result = new FileData{
                data = new() {
                    newData
                }
            };
        }
        else {
            result = readData with {
                data = readData.data.Concat(new List<StatSession> {newData}).ToList()
            };
        }

        return result;
    }
}

[Serializable]
public record FileData {
    public List<StatSession> data;
}

[Serializable]
public record StatSession {
    public string sessionStartDate;
    public string sessionEndDate;
    public List<int> randomGeneration;
    public List<int> answers;
}
