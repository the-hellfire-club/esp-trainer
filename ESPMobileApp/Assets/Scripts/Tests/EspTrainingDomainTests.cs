using System.Collections;
using Events;
using Game;
using NUnit.Framework;
using Tests;
using UnityEngine.TestTools;

public class EspTrainingDomainTests
{
    private EventBusSingleton _eventBus;
    private DataController _dataController;
    private EspTrainingDomain _espTrainingDomain;

    private Hooks _hooks;

    private const int Seed = 10;
    private const int NumberOfTrials = 10;
    private int[] _randomImageValuesOrdered;
    private int[] _randomSeriesValuesOrdered;

    [SetUp]
    public void BeforeEach()
    {
        _randomImageValuesOrdered = new[] { 10, 1, 19, 14, 6, 3, 13, 2, 8, 0, 8 };
        _randomSeriesValuesOrdered = new[] { 0, 1, 1, 3, 0, 2, 2, 3, 2, 0};

        _eventBus = EventBusSingleton.Instance;
        _dataController = new DataController(new RandomGenerator(Seed), NumberOfTrials);
        _espTrainingDomain = new EspTrainingDomain(NumberOfTrials, _eventBus, _dataController);
        _espTrainingDomain.RegisterToEvents();

        _hooks = new Hooks();

        _eventBus.espManagerEvents.scoreChangedEvent.onSent += _hooks.OnScoreChanged;
        _eventBus.uiEvents.passedWithValueEvent.onSent += _hooks.OnPassedWithValue;
        _eventBus.espManagerEvents.newImageGeneratedEvent.onSent += _hooks.OnImageGenerated;
        _eventBus.espManagerEvents.wrongAnswerEvent.onSent += _hooks.OnWrongAnswer;
        _eventBus.espManagerEvents.validAnswerEvent.onSent += _hooks.OnValidAnswer;
        _eventBus.espManagerEvents.endOfSerieAnswerEvent.onSent += _hooks.OnEndOfSerieAnswer;
    }

    [TearDown]
    public void AfterEach()
    {
        _espTrainingDomain.UnregisterToEvents();
        _eventBus.espManagerEvents.scoreChangedEvent.onSent -= _hooks.OnScoreChanged;
        _eventBus.uiEvents.passedWithValueEvent.onSent -= _hooks.OnPassedWithValue;
        _eventBus.espManagerEvents.newImageGeneratedEvent.onSent -= _hooks.OnImageGenerated;
        _eventBus.espManagerEvents.wrongAnswerEvent.onSent -= _hooks.OnWrongAnswer;
        _eventBus.espManagerEvents.validAnswerEvent.onSent -= _hooks.OnValidAnswer;
        _eventBus.espManagerEvents.endOfSerieAnswerEvent.onSent -= _hooks.OnEndOfSerieAnswer;
    }

    [Test]
    public void Should_SaveAndRestart_When_resetEventIsSent()
    {
        _eventBus.uiEvents.resetEvent.RaiseEvent();

        //TODO debug this : bug smell no image generated event sent
        //TODO missing test on saveAndRestart datacontroller
        Assert.True(_hooks.onScoreChangedCalled);
        Assert.AreEqual(0, _hooks.onScoreChangedValue);
        Assert.True(_hooks.onPassedWithValueCalled);
        Assert.AreEqual(-1, _hooks.onPassedWithValueValue);
    }

    [Test]
    public void Should_InitEverything_When_GameStarts()
    {
        _espTrainingDomain.OnStart();
        
        Assert.True(_hooks.onScoreChangedCalled);
        Assert.AreEqual(0, _hooks.onScoreChangedValue);
        Assert.True(_hooks.onImageGeneratedCalled);
        Assert.AreEqual(_randomImageValuesOrdered[0], _hooks.onImageGeneratedValue);
    }

    
    [Test]
    public void Should_PassAndGenerateANewTarget_When_TurnPassed()
    {
        // Arrange
        _espTrainingDomain.OnStart();
        _hooks.ResetHooks();
        
        // Act
        _eventBus.uiEvents.passEvent.RaiseEvent();

        // Assert
        Assert.True(_hooks.onImageGeneratedCalled);
        Assert.AreEqual(_randomImageValuesOrdered[1], _hooks.onImageGeneratedValue);
        Assert.True(_hooks.onPassedWithValueCalled);
        Assert.AreEqual(_randomImageValuesOrdered[1], _hooks.onPassedWithValueValue);
    }
    
    [Test]
    public void Should_RaiseEventsAndComputeScore_When_WrongAnswerIsSelected()
    {
        // Arrange
        _espTrainingDomain.OnStart();

        // Act
        _eventBus.uiEvents.answerSelectedEvent.RaiseEvent(_randomSeriesValuesOrdered[1]);

        // Assert
        Assert.True(_hooks.onScoreChangedCalled);
        Assert.AreEqual(0, _hooks.onScoreChangedValue);
        Assert.True(_hooks.onWrongAnswerCalled);
        Assert.True(_hooks.onImageGeneratedCalled);
        Assert.AreEqual(_randomImageValuesOrdered[1], _hooks.onImageGeneratedValue);
    }
    
    [Test]
    public void Should_RaiseEventsAndComputeScore_When_ValidAnswerIsSelected()
    {
        // Arrange
        _espTrainingDomain.OnStart();

        // Act
        _eventBus.uiEvents.answerSelectedEvent.RaiseEvent(_randomSeriesValuesOrdered[0]);

        // Assert
        Assert.True(_hooks.onScoreChangedCalled);
        Assert.AreEqual(0.1f, _hooks.onScoreChangedValue);
        Assert.True(_hooks.onValidAnswerCalled);
        Assert.True(_hooks.onImageGeneratedCalled);
        Assert.AreEqual(_randomImageValuesOrdered[1], _hooks.onImageGeneratedValue);
    }

    [Test]
    public void Should_Raise_EventsAndComputeScores_When_EndOfSerieIsReached()
    {
        // Arrange
        _espTrainingDomain.OnStart();

        for (int i=0 ; i<NumberOfTrials ; i++)
        {
            _hooks.ResetHooks();
            _eventBus.uiEvents.answerSelectedEvent.RaiseEvent(_randomSeriesValuesOrdered[i]);
            Assert.True(_hooks.onValidAnswerCalled);
            Assert.AreEqual(_randomImageValuesOrdered[i+1], _hooks.onImageGeneratedValue);
        }
        Assert.True(_hooks.onScoreChangedCalled);
        Assert.AreEqual(0, _hooks.onScoreChangedValue);
        Assert.True(_hooks.onEndOfSerieAnswerCalled);
    }

    // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // `yield return null;` to skip a frame.
    [UnityTest]
    public IEnumerator EspWithEnumeratorPasses()
    {
        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        yield return null;
    }
}
