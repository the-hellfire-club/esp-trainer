﻿using NUnit.Framework.Constraints;

namespace Tests
{
    public class Hooks
    {
        public float onScoreChangedValue;
        public bool onScoreChangedCalled;
        
        public int onPassedWithValueValue;
        public bool onPassedWithValueCalled;

        public int onImageGeneratedValue;
        public bool onImageGeneratedCalled;
        
        public bool onWrongAnswerCalled;
        
        public bool onValidAnswerCalled;
        
        public bool onEndOfSerieAnswerCalled;

        public Hooks()
        {
            ResetHooks();
        }

        public void ResetHooks()
        {
            onScoreChangedValue = 0;
            onScoreChangedCalled = false;
        
            onPassedWithValueValue = 0;
            onPassedWithValueCalled = false;

            onImageGeneratedValue = 0;
            onImageGeneratedCalled = false;
            
            onWrongAnswerCalled = false;
            
            onValidAnswerCalled = false;
            
            onEndOfSerieAnswerCalled = false;
        }
        
        public void OnScoreChanged(float value)
        {
            onScoreChangedCalled = true;
            onScoreChangedValue = value;
        }

        public void OnPassedWithValue(int value)
        {
            onPassedWithValueCalled = true;
            onPassedWithValueValue = value;
        }

        public void OnImageGenerated(int value)
        {
            onImageGeneratedValue = value;
            onImageGeneratedCalled = true;
        }
        
        public void OnWrongAnswer()
        {
            onWrongAnswerCalled = true;
        }
        
        public void OnValidAnswer()
        {
            onValidAnswerCalled = true;
        }
        
        public void OnEndOfSerieAnswer()
        {
            onEndOfSerieAnswerCalled = true;
        }
    }
}